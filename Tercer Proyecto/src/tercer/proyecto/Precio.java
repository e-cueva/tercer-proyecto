
package tercer.proyecto;

import java.util.Scanner;


public class Precio {
    
    private String nombre;

    private int identificacion;
    private double pan;
    Scanner data = new Scanner(System.in);

    public Precio(String nombre, int identificacion, int pan) {
        this.nombre = nombre;     
        this.identificacion = identificacion;
        this.pan = pan;
    }
             
    public String getNombre() {
        System.out.println("Nombre del cliente: ");
        nombre = data.nextLine();
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }    
    public int getIdentificacion() {
        System.out.println("Ingrese Cedula del Cliente:");
        identificacion = data.nextInt();
        return identificacion;
    }

    public void setIdentificacion(int identificacion) {
        this.identificacion = identificacion;
    }

    public double getPan() {
        System.out.println("Ingrese numero de panes que compro:");
        pan = data.nextInt();
        return pan*15;
    }

    public void setPan(double pan) {
        pan = pan;
    }

   

}


