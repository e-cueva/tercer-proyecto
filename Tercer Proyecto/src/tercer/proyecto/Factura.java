
package tercer.proyecto;

  
public class Factura extends Precio {

    public Factura(String nombre, int identificacion, int pan) {
        super(nombre, identificacion, pan);
    }
    @Override
    public String toString() {      
        String p = (" NOMBRE DEL CLIENTE : " + getNombre() + "\n CEDULA DEL CLIENTE : " + getIdentificacion() + "\n TOTAL  : $"+ getPan() );
        System.out.println("--------------------------------------------------------");
        return p;
    }
}
